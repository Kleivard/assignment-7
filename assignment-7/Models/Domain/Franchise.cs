﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Models.Domain
{
    /// <summary>
    /// Franchise model.
    /// </summary>
    public class Franchise
    {
        // PK
        public int FranchiseId { get; set; }

        // Fields
        [Required]
        [MaxLength(500)]
        public string Name { get; set; }

        [Required]
        public string Desciption { get; set; }

        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
