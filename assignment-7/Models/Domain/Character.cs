﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Models.Domain
{
    /// <summary>
    /// Character model.
    /// </summary>
    public class Character
    {
        // PK
        public int CharacterId { get; set; }

        // Fields
        [Required]
        [MaxLength(250)]
        public string FullName { get; set; }

        [MaxLength(250)]
        public string Alias { get; set; }

        [Required]
        [MaxLength(50)]
        public string Gender { get; set; }

        [Required]
        [Url]
        public string Picture { get; set; }

        // Relationships
        public ICollection<Movie> Movies { get; set; }
    }
}
