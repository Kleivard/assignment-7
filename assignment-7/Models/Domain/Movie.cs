﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Models.Domain
{
    /// <summary>
    /// Movie model.
    /// </summary>
    public class Movie
    {
        // PK
        public int MovieId { get; set; }

        // Fields
        [Required]
        [MaxLength(250)]
        public string MovieTitle { get; set; }

        [Required]
        [MaxLength(250)]
        public string Genre { get; set; }

        /// <summary>
        /// ReleaseYear is 4 character long numberic value.
        /// </summary>
        [Required]
        [Column(TypeName = "varchar(4)")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Release year must be numeric")]
        public string ReleaseYear { get; set; }

        [Required]
        [MaxLength(250)]
        public string Director { get; set; }

        /// <summary>
        /// Picture is a URL
        /// </summary>
        [Required]
        [Url]
        public string Picture { get; set; }

        /// <summary>
        /// Trailer is a URL
        /// </summary>
        [Url]
        public string Trailer { get; set; }

        // Relationships
        public ICollection<Character> Characters { get; set; }
        public int? FranchiseId { get; set; }
        public Franchise Franchise { get; set; }
    }
}
