﻿using assignment_6.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Models
{
    /// <summary>
    /// Containes 3 methods for seeding data for movies, characters and franchises.
    /// </summary>
    public class SeedHelper
    {
        /// <summary>
        /// Data for movies. Used in DbContext class.
        /// </summary>
        /// <returns>List of Movie obj</returns>
        public static IEnumerable<Movie> GetMovieSeed()
        {
            IEnumerable<Movie> seedMovies = new List<Movie>()
            {
                new Movie() {
                    MovieId = 1,
                    MovieTitle = "Batman v Superman: Dawn of Justice",
                    Genre = "Action",
                    ReleaseYear = "2016",
                    Director = "Zack Snyder",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/2/20/Batman_v_Superman_poster.jpg",
                    Trailer = "https://www.youtube.com/watch?v=4QFPuyDrIIk&ab_channel=HowItShouldHaveEnded",
                    FranchiseId = 1
                },
                new Movie()
                {
                    MovieId = 2,
                    MovieTitle = "Wonder Woman 1984",
                    Genre = "Action",
                    ReleaseYear = "2020",
                    Director = "Patty Jenkins",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/4/4e/Wonder_Woman_1984.png/220px-Wonder_Woman_1984.png",
                    Trailer = "https://www.youtube.com/watch?v=JqRRfUPaNuU&ab_channel=WarnerBros.UK%26Ireland",
                    FranchiseId = 2
                },
                new Movie()
                {
                    MovieId = 3,
                    MovieTitle = "Zack Snyder's Justice League",
                    Genre = "Action",
                    ReleaseYear = "2021",
                    Director = "Zack Snyder",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/6/60/Zack_Snyder%27s_Justice_League.png/220px-Zack_Snyder%27s_Justice_League.png",
                    Trailer = "https://www.youtube.com/watch?v=H7mhNc_6uHI&ab_channel=HBOAsia",
                    FranchiseId = 1
                },
                new Movie()
                {
                    MovieId = 4,
                    MovieTitle = "Hulk",
                    Genre = "Action",
                    ReleaseYear = "2003",
                    Director = "Ang Lee",
                    Picture = "https://upload.wikimedia.org/wikipedia/en/thumb/5/59/Hulk_movie.jpg/220px-Hulk_movie.jpg",
                    Trailer = "https://www.youtube.com/watch?v=5DqnibQlkVc&ab_channel=LegoMan",
                    FranchiseId = 2
                }
            };
            return seedMovies;
        }

        /// <summary>
        /// Data for franchises. Used in DbContext class.
        /// </summary>
        /// <returns>List of franchises</returns>
        public static IEnumerable<Franchise> GetFranchiseSeed()
        {
            IEnumerable<Franchise> seedFranchises = new List<Franchise>()
            {
                new Franchise()
                {
                    FranchiseId = 1,
                    Name = "DC",
                    Desciption = "DC Superheroes"
                },
                new Franchise()
                {
                    FranchiseId = 2,
                    Name = "Marvel",
                    Desciption = "Marvel Superheroes"
                }
            };
            return seedFranchises;
        }

        /// <summary>
        /// Data for characters. Used in DbContext class.
        /// </summary>
        /// <returns>List of characters</returns>
        public static IEnumerable<Character> GetCharacterSeed()
        {
            IEnumerable<Character> seedCharacters = new List<Character>()
            {
            new Character()
            {
                CharacterId = 1,
                FullName = "Batman",
                Alias = "Bruce Wayne",
                Gender = "Male",
                Picture = "https://www.lego.com/cdn/cs/set/assets/blt167d8e20620e4817/DC_-_Character_-_Details_-_Sidekick-Standard_-_Batman.jpg?fit=crop&format=jpg&quality=80&width=800&height=426&dpr=1"
            },
            new Character()
            {
                CharacterId = 2,
                FullName = "Superman",
                Alias = "Clark Kent",
                Gender = "Male",
                Picture = "https://www.lego.com/cdn/cs/set/assets/blt6c65eb54dc9fb50e/DC_-_Character_-_Details_-_Sidekick-Standard_-_Superman.jpg?fit=crop&format=jpg&quality=80&width=800&height=426&dpr=1"
            },
            new Character()
            {
                CharacterId = 3,
                FullName = "Wonder Woman",
                Alias = "Diana Prince",
                Gender = "Female",
                Picture = "https://www.filmtopp.se/media/2017/05/Wonder-Woman-Movie-Artwork.jpeg?w=810&fit=max&s=1f64fd210cd8fe8ef6cd606ca7d1786a"
            },
            new Character()
            {
                CharacterId = 4,
                FullName = "Hulk",
                Alias = "Bruce Banner",
                Gender = "Male",
                Picture = "https://scale.coolshop-cdn.com/product-media.coolshop-cdn.com/235U7E/16024a4f33ec40da8bed442c8b6be1de.png/f/goo-jit-zu-marvel-superhero-giant-supagoo-hulk-40-00758.png"
            }
            };
            return seedCharacters;
        }
    }
}
