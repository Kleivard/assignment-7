﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Models.DTOs.Movie
{
    /// <summary>
    /// DTO for editing/updating movies.
    /// </summary>
    public class MovieEditDTO
    {
        public int MovieId { get; set; }
        public string MovieTitle { get; set; }
        public string Genre { get; set; }
        public string ReleaseYear { get; set; }
        public string Director { get; set; }
        public string Picture { get; set; }
        public string Trailer { get; set; }
        public int? Franchise { get; set; }
    }
}
