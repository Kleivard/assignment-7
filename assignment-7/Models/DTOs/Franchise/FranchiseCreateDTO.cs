﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Models.DTOs.Franchise
{
    /// <summary>
    /// DTO for creating/adding franchise to database.
    /// </summary>
    public class FranchiseCreateDTO
    {
        public string Name { get; set; }
        public string Desciption { get; set; }
    }
}
