﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Models.DTOs.Franchise
{
    /// <summary>
    /// DTO for reading/featching franchises.
    /// </summary>
    public class FranchiseReadDTO
    {
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Desciption { get; set; }

    }
}
