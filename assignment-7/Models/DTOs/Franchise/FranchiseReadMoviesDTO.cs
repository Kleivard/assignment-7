﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_7.Models.DTOs.Franchise
{
    /// <summary>
    /// DTO for reading franchise and associated movies.
    /// </summary>
    public class FranchiseReadMoviesDTO
    {
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Desciption { get; set; }
        public List<int>? Movies { get; set; }
    }
}
