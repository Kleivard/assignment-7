﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Models.DTOs.Franchise
{
    /// <summary>
    /// DTO for reading addition information about Franchise(related movies and characters).
    /// </summary>
    public class FranchiseAdvancedReadDTO
    {
        public int FranchiseId { get; set; }
        public string Name { get; set; }
        public string Desciption { get; set; }
        public List<int>? Movies { get; set; }
        public List<int>? Characters { get; set; }
    }
}
