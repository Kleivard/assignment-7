﻿using assignment_6.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Models.DataAccess
{
    public class MoviesDbContext : DbContext
    {
        public MoviesDbContext(DbContextOptions options) : base(options) { }
        public DbSet<Character> Characters { get; set; }
        public DbSet<Movie> Movies { get; set; }
        public DbSet<Franchise> Franchises { get; set; }

        /// <summary>
        /// Reads in seed data and sets up the many-to-many relations for Movies and Characters
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Movie>().HasData(SeedHelper.GetMovieSeed());
            modelBuilder.Entity<Character>().HasData(SeedHelper.GetCharacterSeed());
            modelBuilder.Entity<Franchise>().HasData(SeedHelper.GetFranchiseSeed());

            modelBuilder.Entity<Character>()
                .HasMany(c => c.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<String, Object>>(
                "CharacterMovie",
                r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                je =>
                {
                    je.HasKey("MovieId", "CharacterId");
                    je.HasData(
                        new { MovieId = 1, CharacterId = 1 },
                        new { MovieId = 1, CharacterId = 2 },
                        new { MovieId = 1, CharacterId = 3 },
                        new { MovieId = 2, CharacterId = 3 },
                        new { MovieId = 3, CharacterId = 1 },
                        new { MovieId = 3, CharacterId = 2 },
                        new { MovieId = 3, CharacterId = 3 },
                        new { MovieId = 4, CharacterId = 4 }
                        );
                });
        }
    }
}
