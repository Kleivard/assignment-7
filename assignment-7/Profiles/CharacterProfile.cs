﻿using assignment_6.Models.Domain;
using assignment_6.Models.DTOs;
using assignment_6.Models.DTOs.Character;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Profiles
{
    /// <summary>
    /// Mapping character to DTOs. Inherits from AutoMapper.Profile
    /// </summary>
    public class CharacterProfile : Profile
    {
        /// <summary>
        /// Mapping character to character DTOs. 
        /// </summary>
        public CharacterProfile()
        {
            // Character <--> CharacterReadDTO
            CreateMap<Character, CharacterReadDTO>().ReverseMap();

            // Character <--> CharacterCreateDTO
            CreateMap<Character, CharacterCreateDTO>().ReverseMap();

            // Character <--> CharacterEditDTO
            CreateMap<Character, CharacterEditDTO>().ReverseMap();
        }
    }
}
