﻿using assignment_6.Models.Domain;
using assignment_6.Models.DTOs.Movie;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Profiles
{
    /// <summary>
    /// Mapping movies to DTOs. Inherits from AutoMapper.Profile
    /// </summary>
    public class MovieProfile : Profile
    {
        /// <summary>
        /// Mapping movie to movie DTOs.
        /// </summary>
        public MovieProfile()
        {
            // Movie <--> MovieReadDTO
            CreateMap<Movie, MovieReadDTO>()
                    .ForMember(mdto => mdto.Franchise, opt => opt
                    .MapFrom(m => m.FranchiseId))
                    .ReverseMap();

            // Movie <--> MovieCreateDTO
            CreateMap<Movie, MovieCreateDTO>()
                    .ForMember(mdto => mdto.Franchise, opt => opt
                    .MapFrom(m => m.FranchiseId))
                    .ReverseMap();

            // Movie <--> MovieEditDTO
            CreateMap<Movie, MovieEditDTO>()
                    .ForMember(mdto => mdto.Franchise, opt => opt
                    .MapFrom(m => m.FranchiseId))
                    .ReverseMap();

            // Movie <--> MovieAdvancedReadDTO
            CreateMap<Movie, MovieAdvancedReadDTO>()
                    .ForMember(mdto => mdto.Franchise, opt => opt
                    .MapFrom(m => m.FranchiseId))
                    .ForMember(mdto => mdto.Characters, opt => opt
                    .MapFrom(m => m.Characters.Select(c => c.CharacterId).ToList()))
                    .ReverseMap();
        }
    }
}
