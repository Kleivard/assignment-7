﻿using assignment_6.Models.Domain;
using assignment_6.Models.DTOs.Franchise;
using assignment_7.Models.DTOs.Franchise;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Profiles
{
    /// <summary>
    /// Mapping franchise to DTOs. Inherits from AutoMapper.Profile
    /// </summary>
    public class FranchiseProfile : Profile
    {
        /// <summary>
        /// Mapping franchise to DTOs.
        /// </summary>
        public FranchiseProfile()
        {
            // Franchise <--> FranchiseReadDTO
            CreateMap<Franchise, FranchiseReadDTO>().ReverseMap();

            // Franchise <--> FranchiseCreateDTO
            CreateMap<Franchise, FranchiseCreateDTO>().ReverseMap();

            // Franchise <--> FranchiseEditDTO
            CreateMap<Franchise, FranchiseEditDTO>().ReverseMap();

            // Franchise <--> FranchiseAdvancedReadDTO
            CreateMap<Franchise, FranchiseAdvancedReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies
                        .Select(m => m.MovieId).ToList()))
                .ForMember(fdto => fdto.Characters, opt => opt
                .MapFrom(f => f.Movies
                        .SelectMany(m => m.Characters
                        .Select(c => c.CharacterId).ToList())))
                .ReverseMap();

            // Franchise <--> FranchiseReadCharactersDTO
            CreateMap<Franchise, FranchiseReadCharactersDTO>()
                .ForMember(fdto => fdto.Characters, opt => opt
                .MapFrom(f => f.Movies
                        .SelectMany(m => m.Characters
                        .Select(c => c.CharacterId).ToList())))
                .ReverseMap();

            // Franchise <--> FranchiseReadMoviesDTO
            CreateMap<Franchise, FranchiseReadMoviesDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies
                        .Select(m => m.MovieId).ToList()))
                .ReverseMap();
        }
    }
}
