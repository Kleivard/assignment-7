﻿using assignment_6.Models.Domain;
using assignment_6.Models.DTOs;
using assignment_6.Models.DTOs.Character;
using assignment_6.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace assignment_6.Controllers
{
    [Route("api/character")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharacterController : ControllerBase
    {
        private readonly IMapper _mapper;
        // service/repositories work with domain objects insted of the controller that work with DTOs
        private readonly ICharacterService _characterService;

        public CharacterController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Fetches all the characters.
        /// </summary>
        /// <returns>List of characters</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return _mapper.Map<List<CharacterReadDTO>>(await _characterService.GetAllCharactersAsync());
        }

        /// <summary>
        /// Fetch a specific character by their id.
        /// </summary>
        /// <param name="id">Characters id.</param>
        /// <returns>Returns a character.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            return _mapper.Map<CharacterReadDTO>(await _characterService.GetSpecificCharacterAsync(id));
        }

        /// <summary>
        /// Updates a character. Takes the id of the character and the full character object in route.
        /// </summary>
        /// <param name="id">Character id</param>
        /// <param name="characterEditDTO">Character obj</param>
        /// <returns>Http status code</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateCharacter(int id, CharacterEditDTO characterEditDTO)
        {
            if (id != characterEditDTO.CharacterId)
            {
                return BadRequest();
            }
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }

            Character domainCharacter = _mapper.Map<Character>(characterEditDTO);
            await _characterService.UpdateCharacterAsync(domainCharacter);

            return NoContent();
        }

        /// <summary>
        /// Adds a new character to the database. Takes a character obj.
        /// </summary>
        /// <param name="characterCreateDTO">Character obj</param>
        /// <returns>Http status code</returns>
        [HttpPost]
        public async Task<ActionResult<Character>> CreateCharacter(CharacterCreateDTO characterCreateDTO)
        {
            Character domainCharacter = _mapper.Map<Character>(characterCreateDTO);

            domainCharacter = await _characterService.AddCharacterAsync(domainCharacter);

            return CreatedAtAction("GetCharacter",
                new { id = domainCharacter.CharacterId },
                _mapper.Map<CharacterReadDTO>(domainCharacter));
        }

        /// <summary>
        /// Deletes a character from the database. Character is identified by id.
        /// </summary>
        /// <param name="id">Character id</param>
        /// <returns>Http status code</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCharacter(int id)
        {
            if (!_characterService.CharacterExists(id))
            {
                return NotFound();
            }
            await _characterService.DeleteCharacterAsync(id);

            return NoContent();
        }

    }
}
