﻿using assignment_6.Models.Domain;
using assignment_6.Models.DTOs.Movie;
using assignment_6.Services;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace assignment_6.Controllers
{
    [Route("api/movies")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]

    public class MovieController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly IMovieService _movieService;

        public MovieController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        /// <summary>
        /// Fetches all movies.
        /// </summary>
        /// <returns>List of movies</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return _mapper.Map<List<MovieReadDTO>>(await _movieService.GetAllMoviesAsync());
        }

        /// <summary>
        /// Fetch specific movie based on movie id.
        /// </summary>
        /// <param name="id">Movie id.</param>
        /// <returns>Movie obj.</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            Movie movie = await _movieService.GetSpecificMovieAsync(id);

            if (movie == null)
            {
                return NotFound();
            }

            return _mapper.Map<MovieReadDTO>(movie);
        }

        /// <summary>
        /// Updates a movie. Takes movie id and the full movie object in route.
        /// </summary>
        /// <param name="id">Movie id.</param>
        /// <param name="movieEditDTO">Movie obj.</param>
        /// <returns>Http status code.</returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, MovieEditDTO movieEditDTO)
        {
            if (id != movieEditDTO.MovieId)
            {
                return BadRequest();
            }

            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            Movie domainMovie = _mapper.Map<Movie>(movieEditDTO);
            await _movieService.UpdateMovieAsync(domainMovie);

            return NoContent();
        }

        /// <summary>
        /// Adds a movie to the database. Takes a movie obj in route.
        /// </summary>
        /// <param name="movieCreateDTO">Movie obj (franchise can be null)</param>
        /// <returns>Http status code.</returns>
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(MovieCreateDTO movieCreateDTO)
        {
            Movie domainMoive = _mapper.Map<Movie>(movieCreateDTO);

            domainMoive = await _movieService.AddMovieAsync(domainMoive);

            return CreatedAtAction("GetMovie",
                new {id = domainMoive.MovieId },
                _mapper.Map<MovieReadDTO>(domainMoive));
        }

        /// <summary>
        /// Deletes a specific movie from the database. Movie is identified by id.
        /// </summary>
        /// <param name="id">Movie id.</param>
        /// <returns>Http status code.</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteMovie(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            await _movieService.DeleteMovieAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Updates movie with franchise. Takes movie id and franchise id in route.
        /// </summary>
        /// <param name="id">Movie id.</param>
        /// <param name="franchiseId">Franchise id.</param>
        /// <returns>Http status code.</returns>
        [HttpPut("{id}/add-franchise/{franchiseId}")]
        public async Task<IActionResult> UpdateMovieFranchiseAdd(int id, int franchiseId)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.UpdateMovieFranchiseAsync(id, franchiseId);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Franchise not Found");
            }

            return NoContent();
        }

        /// <summary>
        /// Removes franchise from movie. Takes movie id in route.
        /// </summary>
        /// <param name="id">Movie id.</param>
        /// <returns>Http status code.</returns>
        [HttpPut("{id}/remove-franchise")]
        public async Task<IActionResult> UpdateMovieFranchiseDelete(int id)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.RemoveFranchiseFromMovie(id);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Franchise not Found");
            }

            return NoContent();
        }

        /// <summary>
        /// Removes character from movie. Takes movie id in route.
        /// </summary>
        /// <param name="id">Movie id.</param>
        /// <param name="characterId">Character id.</param>
        /// <returns>Http status code.</returns>
        [HttpPut("{id}/remove-character/{characterId}")]
        public async Task<IActionResult> RemoveCharacterFromMovie(int id, int characterId)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }
            try
            {
                await _movieService.RemoveCharacterFromMovie(id, characterId);
            }
            catch (KeyNotFoundException)
            {
                return NotFound("Character not Found");
            }
            catch (ArgumentException)
            {
                return StatusCode(406);
            }

            return NoContent();
        }

        /// <summary>
        /// Add character to a movie. Takes movie id and character id in route.
        /// </summary>
        /// <param name="id">Movie id.</param>
        /// <param name="characterId">Character id.</param>
        /// <returns>Http status code.</returns>
        [HttpPut("{id}/add-character-to-movie/{characterId}")]
        public async Task<IActionResult> AddCharacterToMovie(int id, int characterId)
        {
            if (!_movieService.MovieExists(id))
            {
                return NotFound();
            }

            try
            {
                await _movieService.AddCharacterToMovieAsync(id, characterId);
            }
            catch (KeyNotFoundException)
            {
                return BadRequest("Character not Found");
            }

            return NoContent();
        }

        /// <summary>
        /// Fetch all movies with associated characters.
        /// </summary>
        /// <returns>List if movies</returns>
        [HttpGet("advanced-read")]
        public async Task<ActionResult<IEnumerable<MovieAdvancedReadDTO>>> GetMoviesAdvanced()
        {
            return _mapper.Map<List<MovieAdvancedReadDTO>>(await _movieService.GetAllMoviesAdvancedAsync());
        }
    }
}
