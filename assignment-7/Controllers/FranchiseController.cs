using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using assignment_6.Models.Domain;
using System.Net.Mime;
using AutoMapper;
using assignment_6.Services;
using assignment_6.Models.DTOs.Franchise;
using assignment_7.Models.DTOs.Franchise;

namespace assignment_6.Controllers
{
    [Route("api/franchise")]
    [ApiController]
    [Produces(MediaTypeNames.Application.Json)]
    [Consumes(MediaTypeNames.Application.Json)]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly IFranchiseService _franchiseService;

        public FranchiseController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }

        /// <summary>
        /// Fetch a specific franchise by their id.
        /// </summary>
        /// <param name="id">Franchise id.</param>
        /// <returns>Returns a franchise</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            return _mapper.Map<FranchiseReadDTO>(await _franchiseService.GetSpecificFranchiseAsync(id));
        }

        /// <summary>
        /// Updates a franchise. Takes the id and the full franchise object in route.
        /// </summary>
        /// <param name="id">Franchise id</param>
        /// <param name="franchiseEditDTO">Franchise obj</param>
        /// <returns>Http status code</returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateFranchise(int id, FranchiseEditDTO franchiseEditDTO)
        {
            if (id != franchiseEditDTO.FranchiseId)
            {
                return BadRequest();
            }
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseEditDTO);
            await _franchiseService.UpdateFranchiseAsync(domainFranchise);

            return NoContent();
        }

        /// <summary>
        /// Adds a franchise and inserts it to the database.
        /// </summary>
        /// <param name="franchiseCreateDTO">Franchise obj.</param>
        /// <returns>Http status code.</returns>
        [HttpPost]
        public async Task<ActionResult<Franchise>> CreateFranchise(FranchiseCreateDTO franchiseCreateDTO)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(franchiseCreateDTO);

            domainFranchise = await _franchiseService.AddFranchiseAsync(domainFranchise);

            return CreatedAtAction("GetFranchise",
                new { id = domainFranchise.FranchiseId },
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        /// <summary>
        /// Delets a franchise. Takes franchise id as identifier.
        /// </summary>
        /// <param name="id">Franchise id.</param>
        /// <returns>Http status code.</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteFranchise(int id)
        {
            if (!_franchiseService.FranchiseExists(id))
            {
                return NotFound();
            }
            await _franchiseService.DeleteFranchiseAsync(id);

            return NoContent();
        }

        /// <summary>
        /// Feteches all franchises.
        /// </summary>
        /// <returns>List of franchises.</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllFranchisesAsync());
        }

        /// <summary>
        /// Fetches all franchises and the associated movies with characters.
        /// </summary>
        /// <returns>List of franchises.</returns>
        [HttpGet("advanced-read")]
        public async Task<ActionResult<IEnumerable<FranchiseAdvancedReadDTO>>> GetFranchisesAdvanced()
        {
            return _mapper.Map<List<FranchiseAdvancedReadDTO>>(await _franchiseService.GetAllFranchisesAdvancedAsync());
        }

        /// <summary>
        /// Fetches all franchises and associated movies.
        /// </summary>
        /// <returns>List of franchises.</returns>
        [HttpGet("movies-in-franchise")]
        public async Task<ActionResult<IEnumerable<FranchiseReadMoviesDTO>>> GetAllMoviesInFranchise()
        {
            return _mapper.Map<List<FranchiseReadMoviesDTO>>(await _franchiseService.GetAllCharactersInFranchiseAsync());
        }

        /// <summary>
        /// Fetches all franchises and associated characters.
        /// </summary>
        /// <returns>List of franchises.</returns>
        [HttpGet("characters-in-franchise")]
        public async Task<ActionResult<IEnumerable<FranchiseReadCharactersDTO>>> GetAllCharactersInFranchise()
        {
            return _mapper.Map<List<FranchiseReadCharactersDTO>>(await _franchiseService.GetAllCharactersInFranchiseAsync());
        }
    }
}
