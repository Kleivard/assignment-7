﻿using assignment_6.Models.DataAccess;
using assignment_6.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Services
{
    /// <summary>
    /// Service class for movies implements IMovieService operations.
    /// </summary>
    public class MovieService : IMovieService
    {
        private readonly MoviesDbContext _context;
        public MovieService(MoviesDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds a movie to the database. Takes domain movie as param.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public async Task<Movie> AddMovieAsync(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        /// <summary>
        /// Deletes movie from database. Takes movie id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            _context.Movies.Remove(movie);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Fetches all movies.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAsync()
        {
            return await _context.Movies.ToListAsync();
        }

        /// <summary>
        /// Fetches a specific movie. Takes movie id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Movie> GetSpecificMovieAsync(int id)
        {
            var movie = await _context.Movies.FindAsync(id);
            return movie;
        }

        /// <summary>
        /// Checks if movie exist. Takes movie id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }

        /// <summary>
        /// Updates a movie. Takes domain movie as param.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public async Task UpdateMovieAsync(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Updates/adds franchise to movie. Takes movie id and franchse id as param.
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public async Task UpdateMovieFranchiseAsync(int movieId , int franchiseId)
        {
            try
            {
                var movie = await _context.Movies.FindAsync(movieId);
                if (_context.Franchises.Any(e => e.FranchiseId == franchiseId))
                {
                    movie.FranchiseId = franchiseId;
                    _context.Entry(movie).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception)
            {

                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Removes franchise from movie. Takes movie id and franchise id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task RemoveFranchiseFromMovie(int id)
        {
            try
            {
                var movie = await _context.Movies.FindAsync(id);
                movie.FranchiseId = null;
                _context.Entry(movie).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {

                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Removes character from movie. Takes movie id and character id as param. (WIP)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public async Task RemoveCharacterFromMovie(int id, int characterId)
        {
            try
            {
                var movie = await _context.Movies.Include(p => p.Characters).SingleAsync(p => p.MovieId == id);
                var characterToRemove = await _context.Characters.SingleAsync(p => p.CharacterId == characterId);

                if(movie.Characters.Contains(characterToRemove))
                {
                    movie.Characters.Remove(characterToRemove);
                    await _context.SaveChangesAsync();
                }
                else
                {
                    throw new ArgumentException();
                }
            }
            catch (ArgumentException)
            {
                throw new ArgumentException();
            }
            catch (Exception)
            {

                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Adds a character to a movie. Takes movie id and character id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public async Task AddCharacterToMovieAsync(int id, int characterId)
        {
            try
            {
                var movie = await _context.Movies.Include(p => p.Characters).SingleAsync(p => p.MovieId == id);

                var existingCharacter = await _context.Characters.SingleAsync(c => c.CharacterId == characterId);

                movie.Characters.Add(existingCharacter);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {

                throw new KeyNotFoundException();
            }
        }

        /// <summary>
        /// Fetches all movies with associated characters.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Movie>> GetAllMoviesAdvancedAsync()
        {
            return await _context.Movies.Include(m => m.Characters).ToListAsync();
        }
    }
}
