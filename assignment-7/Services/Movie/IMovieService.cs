﻿using assignment_6.Models.Domain;
using assignment_6.Models.DTOs.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Services
{
    /// <summary>
    /// Movie serivce interface handels domain operations.
    /// </summary>
    public interface IMovieService
    {
        /// <summary>
        /// Fetches all movies.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetAllMoviesAsync();

        /// <summary>
        /// Fetches a specific movie. Takes movie id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Movie> GetSpecificMovieAsync(int id);

        /// <summary>
        /// Adds a movie to the database. Takes domain movie as param.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public Task<Movie> AddMovieAsync(Movie movie);
        
        /// <summary>
        /// Updates a movie. Takes domain movie as param.
        /// </summary>
        /// <param name="movie"></param>
        /// <returns></returns>
        public Task UpdateMovieAsync(Movie movie);

        /// <summary>
        /// Updates/adds franchise to movie. Takes movie id and franchse id as param.
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="franchiseId"></param>
        /// <returns></returns>
        public Task UpdateMovieFranchiseAsync(int movieId, int franchiseId);
        
        /// <summary>
        /// Deletes movie from database. Takes movie id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteMovieAsync(int id);
        
        /// <summary>
        /// Checks if movie exist. Takes movie id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool MovieExists(int id);
        
        /// <summary>
        /// Removes character from movie. Takes movie id and character id as param. (WIP)
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public Task RemoveCharacterFromMovie(int id, int characterId);

        /// <summary>
        /// Removes franchise from movie. Takes movie id and franchise id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public Task RemoveFranchiseFromMovie(int id);
        
        /// <summary>
        /// Fetches all movies with associated characters.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Movie>> GetAllMoviesAdvancedAsync();

        /// <summary>
        /// Adds a character to a movie. Takes movie id and character id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterId"></param>
        /// <returns></returns>
        public Task AddCharacterToMovieAsync(int id, int characterId);
    }
}
