﻿using assignment_6.Models.DataAccess;
using assignment_6.Models.Domain;
using assignment_6.Services;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Services
{
    /// <summary>
    /// Service class for franchise implements IFranchiseService operations.
    /// </summary>
    public class FranchiseService : IFranchiseService
    {
        private readonly MoviesDbContext _context;

        public FranchiseService(MoviesDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds a franchise to the database.
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public async Task<Franchise> AddFranchiseAsync(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        /// <summary>
        /// Checks if franchise exist. Takes franchise id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(c => c.FranchiseId == id);
        }

        /// <summary>
        /// Deletes a franchise. Takes franchise id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteFranchiseAsync(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Fetch specific franchise. Takes franchise id as para,
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Franchise> GetSpecificFranchiseAsync(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }

        /// <summary>
        /// Updates a franchise. Takes domain franchise as param.
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public async Task UpdateFranchiseAsync(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Fetches all franchises.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAsync()
        {
            return await _context.Franchises.ToListAsync();
        }

        /// <summary>
        /// Fetches all franchises with additinal information.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Franchise>> GetAllFranchisesAdvancedAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ThenInclude(m => m.Characters).ToListAsync();
        }

        /// <summary>
        /// Fetches all franchises and associated movies.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Franchise>> GetAllMoviesInFranchiseAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ToListAsync();
        }

        /// <summary>
        /// Fetches all franchises and associated characters.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Franchise>> GetAllCharactersInFranchiseAsync()
        {
            return await _context.Franchises.Include(f => f.Movies).ThenInclude(m => m.Characters).ToListAsync();
        }
    }
}
