﻿using assignment_6.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Services
{
    /// <summary>
    /// Franchise serivce interface handels domain operations.
    /// </summary>
    public interface IFranchiseService
    {
        /// <summary>
        /// Fetch specific franchise. Takes franchise id as para,
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Franchise> GetSpecificFranchiseAsync(int id);

        /// <summary>
        /// Adds a franchise to the database.
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public Task<Franchise> AddFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Updates a franchise. Takes domain franchise as param.
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns></returns>
        public Task UpdateFranchiseAsync(Franchise franchise);

        /// <summary>
        /// Deletes a franchise. Takes franchise id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteFranchiseAsync(int id);

        /// <summary>
        /// Checks if franchise exist. Takes franchise id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool FranchiseExists(int id);

        /// <summary>
        /// Fetches all franchises.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Franchise>> GetAllFranchisesAsync();

        /// <summary>
        /// Fetches all franchises with additinal information.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Franchise>> GetAllFranchisesAdvancedAsync();

        /// <summary>
        /// Fetches all franchises and associated movies.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Franchise>> GetAllMoviesInFranchiseAsync();

        /// <summary>
        /// Fetches all franchises and associated characters.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Franchise>> GetAllCharactersInFranchiseAsync();
    }
}
