﻿using assignment_6.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Services
{
    /// <summary>
    /// Character serivce interface handels domain operations.
    /// </summary>
    public interface ICharacterService
    {
        /// <summary>
        /// Fetches all characters.
        /// </summary>
        /// <returns></returns>
        public Task<IEnumerable<Character>> GetAllCharactersAsync();

        /// <summary>
        /// Fetch specific character.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task<Character> GetSpecificCharacterAsync(int id);

        /// <summary>
        /// Adds character to database.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task<Character> AddCharacterAsync(Character character);

        /// <summary>
        /// Updates character.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public Task UpdateCharacterAsync(Character character);

        /// <summary>
        /// Delets character.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Task DeleteCharacterAsync(int id);

        /// <summary>
        /// Checks if character exists. Takes id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CharacterExists(int id);
    }
}
