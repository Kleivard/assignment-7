﻿using assignment_6.Models.DataAccess;
using assignment_6.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace assignment_6.Services
{
    /// <summary>
    /// Service class for character implements ICharacterService operations.
    /// </summary>
    public class CharacterService : ICharacterService
    {
        private readonly MoviesDbContext _context;

        public CharacterService(MoviesDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Adds character to database.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task<Character> AddCharacterAsync(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }

        /// <summary>
        /// Checks if character exists. Takes id as param.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(c => c.CharacterId == id);
        }

        /// <summary>
        /// Delets character.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task DeleteCharacterAsync(int id)
        {
            var character = await _context.Characters.FindAsync(id);
            _context.Characters.Remove(character);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Fetches all characters.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Character>> GetAllCharactersAsync()
        {
            return await _context.Characters.ToListAsync();
        }

        /// <summary>
        /// Fetch specific character.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Character> GetSpecificCharacterAsync(int id)
        {
            return await _context.Characters.FindAsync(id);
        }

        /// <summary>
        /// Updates character.
        /// </summary>
        /// <param name="character"></param>
        /// <returns></returns>
        public async Task UpdateCharacterAsync(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
    }
}
